//Criando array de Heroes;
const heroesC = [
  { name: "Darkwing Duck" },
  { name: "Hong Kong Fu" },
  { name: "Os Impossiveis" },
];

//chamando item que será preenchido no html;
const herocallbacklist = document.getElementById("herocallbacklist");

//Criando function para preencher lista de heroes;
const getHeroesCallback = () => {
  setTimeout(() => {
    let output = "";
    heroesC.forEach((item, index) => {
      output += `<li>${item.name}</li>`;
    });
    herocallbacklist.innerHTML = output;
  }, 1000);
};

//Function para adicionar um novo hero e utilizando
//callback para executar a getHeroes depois da execução
//dessa function;
const addHeroCallback = (name, callback) => {
  setTimeout(() => {
    heroesC.push({ name });
    callback();
  }, 2000);
};

//executando a function addHero passando a function
//getHeroes como parametro callback;
addHeroCallback("Formiga Atomica", getHeroesCallback);
