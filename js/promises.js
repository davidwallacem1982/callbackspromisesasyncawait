//Criando array de Heroes;
const heroesP = [
  { name: "Darkwing Duck" },
  { name: "Hong Kong Fu" },
  { name: "Os Impossiveis" },
];

//chamando item que será preenchido no html;
const heropromiselist = document.getElementById("heropromiselist");

//Criando function para preencher lista de heroes;
const getHeroesPromise = () => {
  setTimeout(() => {
    let output = "";
    heroesP.forEach((item, index) => {
      output += `<li>${item.name}</li>`;
    });
    heropromiselist.innerHTML = output;
  }, 1000);
};

//Function para adicionar um novo hero e utilizando
//callback para executar a getHeroes depois da execução
//dessa function;
const addHeroPromise = (name) => {
  const callback = (resolve, reject) => {
    setTimeout(() => {
      heroesP.push({ name });
      resolve();
    }, 2000);
  };

  return new Promise(callback);
};

//executando a function addHero que retorna um Promise
addHeroPromise("Formiga Atomica")
  .then(() => {
    return addHeroPromise("Space Ghost");
  })
  .then(getHeroesPromise)
  .finally(() => {
    console.log("All heroes added");
  });
