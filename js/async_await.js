//Criando array de Heroes;
const heroesAA = [
  { name: "Darkwing Duck" },
  { name: "Hong Kong Fu" },
  { name: "Os Impossiveis" },
];

//chamando item que será preenchido no html;
const heroasyncawaitlist = document.getElementById("heroasyncawaitlist");

//Criando function para preencher lista de heroes;
const getHeroesAsyncAwait = () => {
  setTimeout(() => {
    let output = "";
    heroesAA.forEach((item, index) => {
      output += `<li>${item.name}</li>`;
    });
    heroasyncawaitlist.innerHTML = output;
  }, 1000);
};

//Function para adicionar um novo hero e utilizando
//callback para executar a getHeroes depois da execução
//dessa function;
const addHeroAsyncAwait = (name) => {
  const callback = (resolve, reject) => {
    setTimeout(() => {
      heroesAA.push({ name });

      resolve();
    }, 3000);
  };

  return new Promise(callback);
};

//executando a function addHero que retorna um Promise
const runAsyncMethods = async () => {
  try {
    await addHeroAsyncAwait("My Thor");
    await addHeroAsyncAwait("Homem Passaro");
    getHeroesAsyncAwait();
  } catch (error) {
    console.log(error);
  }
};

runAsyncMethods();
